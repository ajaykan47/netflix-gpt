// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD57VTfcZtQelqBHCXmDtKSradvjgVQSnE",
  authDomain: "netflixgpt-d4ca1.firebaseapp.com",
  projectId: "netflixgpt-d4ca1",
  storageBucket: "netflixgpt-d4ca1.appspot.com",
  messagingSenderId: "48571545341",
  appId: "1:48571545341:web:f19d776a6b0f50e31a7094",
  measurementId: "G-FYX513QRGV"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const authFirebase = getAuth();