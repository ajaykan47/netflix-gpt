
export const LOGO = "https://cdn.cookielaw.org/logos/dd6b162f-1a32-456a-9cfe-897231c7763c/4345ea78-053c-46d2-b11e-09adaef973dc/Netflix_Logo_PMS.png";

export const USER_AVATAR = "";

export const API_OPTIONS = {
    method: "GET",
	headers: {
		accept: 'application/json',
		Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3Mjc1Y2MxODgyM2RiODBhMmVmNDQxNjIzMzNlNmI0YiIsIm5iZiI6MTcxOTIxMTMwMS44OTM5NTYsInN1YiI6IjY2NzkxNDQ5MDI4ZDk0NTU1MGYyNDExMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.QXCqqTKslKM1B7sOiOPFELO12JhWRGHLDnDgKe_EwRA'
	  }
}
export const IMG_CDN_URL= "https://image.tmdb.org/t/p/w400/";
