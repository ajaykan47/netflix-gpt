import { useEffect } from "react";
import { API_OPTIONS } from "../utils/constants";
import { addTrailerVideo } from "../utils/moviesSlice";
import {useDispatch} from "react-redux";

const useMovieTrailer = (movieId) => {
    const dispatch = useDispatch() //second solution
  // fetch the trailer video and updating the store with trailer video data
  const getMoviesVideo = async () => {
    const data = await fetch(
      "https://api.themoviedb.org/3/movie/"+movieId+"/videos?language=en-US",
      API_OPTIONS
    );
    const moviesVideo = await data.json();

    const filterTrailerData = moviesVideo.results.filter(
      (video) => video.type === "Trailer"
    );
    //console.log("filter data of trailer=>", filterTrailerData);
    //if filter data exist then soo filter data otherwise we will display other data.
    const trailer = filterTrailerData.length
      ? filterTrailerData[0]
      : moviesVideo.results[0];
    //setTrailerId(trailer.key) // first solution or keep in redux store
    dispatch(addTrailerVideo(trailer));
  };

  useEffect(() => {
    getMoviesVideo();
  }, []);
};
export default useMovieTrailer;
