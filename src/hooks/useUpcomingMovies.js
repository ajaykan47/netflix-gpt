import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { addUpcomingMovies } from "../utils/moviesSlice";
import { API_OPTIONS } from "../utils/constants";

const useUpcomingMovies = () => {
  const dispatch = useDispatch();

  const getUpcomingMovies = async () => {
  try {
    const data = await fetch(
      "https://api.themoviedb.org/3/movie/upcoming?language=en-US&page=1",
      API_OPTIONS
    );
    const movieJson = await data.json();
    dispatch(addUpcomingMovies(movieJson));
  } catch (error) {
    console.log('hook::useUpcoming', error);
  }
  };

  useEffect(() => {
    getUpcomingMovies();
    return () => {};
  }, []);
};

export default useUpcomingMovies;
