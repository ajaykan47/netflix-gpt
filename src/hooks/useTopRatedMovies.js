import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { addTopRatedMovies } from "../utils/moviesSlice";
import { API_OPTIONS } from "../utils/constants";

const useTopRatedMovies = () => {
  const dispatch = useDispatch();

  const getTopRatedrMovies = async () => {
  try {
    const data = await fetch(
      "https://api.themoviedb.org/3/movie/top_rated?language=en-US&page=1",
      API_OPTIONS
    );
    const movieJson = await data.json();
    dispatch(addTopRatedMovies(movieJson));
  } catch (error) {
    console.log('hook::topRatedMovies', error);
  }
  };

  useEffect(() => {
    getTopRatedrMovies();
    return () => {};
  }, []);
};

export default useTopRatedMovies;
