import React, { useEffect } from "react";
import { signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { onAuthStateChanged } from "firebase/auth";
import { authFirebase } from "../utils/firebase";
import { useDispatch } from "react-redux";
import { addUser, removeUser } from "../utils/userSlice";
import {LOGO} from "../utils/constants";

const Header = () => {
  const user = useSelector((store) => store.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSignOut = () => {
    signOut(authFirebase)
      .then(() => {
        // Sign-out successful.
      })
      .catch((error) => {
        navigate("/error-404");
      });
  };

  //use once
  useEffect(() => {
    onAuthStateChanged(authFirebase, (user) => {
      if (user) {
        // User is signed in
        const { uid, email, displayName, photoURL } = user;
        dispatch(
          addUser({
            uid: uid,
            email: email,
            displayName: displayName,
            photoURL: photoURL,
          })
        );
        navigate("/browse");
      } else {
        // User is signed out
        dispatch(removeUser());
        navigate("/");
      }
    });
  }, []);

  return (
    <div className="absolute w-screen px-8 py-2 bg-gradient-to-b from-black z-10 flex justify-between">
      <header className="text-white container mx-auto flex items-center justify-between p-4">
        <div>
        <img
        className="w-44"
        src={LOGO}
        alt="logo"
      />
        </div>
        <nav className="flex space-x-4">
          <a href="#" className="hover:text-gray-300">
            Home
          </a>
          <a href="#" className="hover:text-gray-300">
            TV Shows
          </a>
          <a href="#" className="hover:text-gray-300">
            Movies
          </a>
          <a href="#" className="hover:text-gray-300">
            My List
          </a>

          { user && (
          <div className="relative group">
            <button className="focus:outline-none">
              <img className="rounded-lg w-12 h-10" src={user?.photoURL} alt="user-icon" />
            </button>
            <ul className="absolute hidden group-hover:block w-40 p-2 h-20 bg-gray-800 text-white rounded-md shadow-md">
              <li>
              <button
                  className="font-bold text-white"

                >
                  {"Account"}
                </button>
              </li>
              <li>
                <button
                  className="font-bold text-white"
                  onClick={handleSignOut}
                >
                  {"Sign Out"}
                </button>
              </li>
            </ul>
          </div>
          )}
        </nav>
      </header>
    </div>
  );
};

export default Header;
