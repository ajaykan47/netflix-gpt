import React from "react";

const VideoTitle = ({ title, overview }) => {
  return (
    <>
      <div className="w-screen aspect-video pt-[20%] px-12 absolute text-white bg-gradient-to-r from-black">
        <h2 className="text-6xl font-bold">{title}</h2>
        <p className="py-6 text-lg w-1/4">{overview}</p>
        <div className="">
            <button className="bg-white text-black px-12 p-4 text-xl rounded-lg hover:bg-opacity-80">
                Play
                </button>
            <button className="rounded-lg mx-2 text-white bg-gray-500 px-16 p-4 text-lg gap-4 hover:bg-opacity-80">More Info </button>
            </div>
      </div>
    </>
  );
};

export default VideoTitle;
